var openBrowser = require('open-browser-webpack-plugin');

module.exports = {
	entry: './src/main.js',
	output: {
		path: __dirname,
		filename: 'bin/bundle.js'
	},
	module:{
		loaders: [
			{
				test: /\.vue$/,
				exclude: /node_modules/,
				loader: 'vue'
			},
		]
	},
	plugins: [
        new openBrowser({
        	url: 'http://localhost:8080',
        })
    ]
}